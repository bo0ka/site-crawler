# -*- coding: utf-8 -*-

DEBUG = True

SERVER_HOST = '0.0.0.0'
SERVER_PORT = 8030

LOG_CONFIG = 'logging.yaml'

JWT_SECRET = "ljhsdY*&HdDQSDdf!54365e5xsFDHG&^$$--bWUEsdz11e*&^fIHDW78E23*&%*&^rtfgtf%^$%$^&SZFD"

USE_DISPLAY = False

MONGO_HOST = '192.168.101.235'
MONGO_DB = 'api'
MONGO_URI = 'mongodb://{}'.format(MONGO_HOST)
MONGO_API_TASK_COLLECTION = 'task'
MONGO_USER_COLLECTION = 'user'

RABBIT_HOST = '192.168.101.235'
RABBIT_PORT = 5672
RABBIT_USER = 'statservice'
RABBIT_PASSWORD = 'tdhjvtl2016'

APIS = [
    'apps.apis.site_crawler'
]

BOTS = [
    'apps.bots.site_crawler_bot'
]
