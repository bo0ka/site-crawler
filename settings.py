# -*- coding: utf-8 -*-

import os
import sys

BASEDIR = os.path.dirname(__file__)
sys.path.insert(0, BASEDIR)

DEBUG = False

SERVER_HOST = '0.0.0.0'
SERVER_PORT = 8030

JWT_SECRET = "secret"
JWT_ALGORITHM = 'HS256'

MONGO_HOST = ''
MONGO_DB = ''
MONGO_URI = 'mongodb://{}'.format(MONGO_HOST)
MONGO_API_TASK_COLLECTION = 'task'
MONGO_USER_COLLECTION = 'user'

RABBIT_HOST = ''
RABBIT_PORT = 5672
RABBIT_USER = ''
RABBIT_PASSWORD = ''

API_VERSION = 'v1'

APIS = []

BOTS = []

SERVICES = []

USE_DISPLAY = True
VNC_PORT = 5603

try:
    from settings_deploy import *
except ImportError:
    pass
try:
    from settings_local import *
except ImportError:
    pass

LOG_CONFIG_PATH = os.path.join(BASEDIR, 'config', LOG_CONFIG)
