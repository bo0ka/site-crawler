# -*- coding: utf-8 -*-


METHODS = ['get', 'post', 'put', 'delete']


class HTTPMethodNotAllowed(Exception):
    def __init__(self, method):
        self.method = method
        self.allowed_methods = METHODS


class HTTPNotFound(Exception):
    pass


def route(path, method='GET'):

    def wrapper(inst):
        _path = path + '/' if not path.endswith('/') else path
        _path = _path.lower()
        if _path not in Routes.routes:
            Routes.routes[_path] = {}
        _method = method.lower()
        if _method not in METHODS:
            raise Exception('Method must be one of {}.'.format(METHODS))
        Routes.routes[_path][_method] = inst
        return inst
    return wrapper


class Routes(object):
    routes = {}

    def call(self, route, method, request, api):
        route, method = route.lower(), method.lower()
        if route not in Routes.routes:
            raise HTTPNotFound()
        if method not in Routes.routes[route]:
            raise HTTPMethodNotAllowed(method=method)
        return Routes.routes[route][method](self, request, api)
