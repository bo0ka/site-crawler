from aiohttp import web
# import jwt
import settings


async def get_user(app, username):
    user = await app.user_collection.find_one({'username': username})
    if user:
        return username
    return None

async def auth_middleware(app, handler):
    async def middleware(request):
        # request.user = None
        # jwt_token = request.headers.get('authorization', None)
        # if jwt_token:
        #     try:
        #         payload = jwt.decode(jwt_token, settings.JWT_SECRET, algorithms=[settings.JWT_ALGORITHM])
        #     except (jwt.DecodeError, jwt.ExpiredSignatureError):
        #         raise web.HTTPForbidden()
        #     user = await get_user(app, payload['user'])
        #     if user:
        #         request.user = payload['user']
        #     else:
        #         raise web.HTTPForbidden()
        return await handler(request)
    return middleware