from cerberus import Validator as JsonValidator


class ValidationException(Exception):
    def __init__(self, errors, json_schema, message=None):
        self.errors = errors
        self.json_schema = json_schema
        self.error_dict = {'message': message if message else 'Wrong input params.',
                           'valid_params': self.json_schema,
                           'errors': self.errors}


class ValidatorBase:
    json_schema = None
    json_allow_unknown = None
    request_type = None

    def __init__(self, json_schema=None, json_allow_unknown=None, request_type='web'):
        self.json_schema = json_schema
        self.json_allow_unknown = json_allow_unknown
        self.request_type = request_type
        self.jv = JsonValidator(allow_unknown=self.json_allow_unknown, schema=self.json_schema)

    async def valid_params(self, request):
        raise Exception('Must implement valid_params().')

    async def validate_request(self, request):
        if self.request_type == 'web':
            try:
                params = await request.json()
            except Exception as e:
                raise ValidationException(None, self.json_schema, message="Can't decode json.")
            return params
        else:
            return request
