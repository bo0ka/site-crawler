# -*- coding: utf-8 -*-


class MethodNotFound(Exception):
    pass


def method(method='STATUS'):
    def wrapper(inst):
        _method = method.lower()
        if _method not in Methods.methods:
            Methods.methods[method] = {}
        Methods.methods[_method] = inst
        return inst
    return wrapper


class Methods(object):
    methods = {}

    def call(self, method, request, service):
        if method not in Methods.methods:
            raise MethodNotFound()
        return self.__class__.methods[method](self, request, service)
