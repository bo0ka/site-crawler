# -*- coding: utf-8 -*-

CELERY_STATUS_EXCHANGE = 'statservice.site_crawler'
CELERY_TASK = 'statservice.task.site_crawler_status'
CELERY_MESSAGE = {"id": '', "task": CELERY_TASK, "kwargs": {"task": '', "status": ""}}

MONGO_HOST = '192.168.101.235'
MONGO_DB = 'site_crawler'
MONGO_URI = 'mongodb://{}/{}'.format(MONGO_HOST, MONGO_DB)
MONGO_REQUEST_COLLECTION = 'request'
MONGO_FS_DB = 'site_crawler_fs'
MONGO_FS_URI = 'mongodb://{}/{}'.format(MONGO_HOST, MONGO_FS_DB)
