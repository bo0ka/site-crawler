import datetime

from core.validation import ValidatorBase, ValidationException
from .settings import JSON_SCHEMA, JSON_ALLOW_UNKNOWN


class Validator(ValidatorBase):
    def __init__(self):
        super(Validator, self).__init__(json_schema=JSON_SCHEMA, json_allow_unknown=JSON_ALLOW_UNKNOWN)

    async def valid_params(self, request):
        params = await self.validate_request(request)
        if not self.jv.validate(params):
            raise ValidationException(self.jv.errors, self.json_schema)
        params.update({
            'requests_complete': [], 'requests_error': [], 'status': 'process',
            'date': datetime.datetime.now(),
            "force_fetch": params.get("force_fetch", False),
            "images": params.get("images", False)
            # "wait": params.get("wait", False)
        })
        return params
