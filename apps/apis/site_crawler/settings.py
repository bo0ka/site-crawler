# -*- coding: utf-8 -*-

from settings import *

XML_RPC_CLIENT = 'tcp://127.0.0.1:5558'

CELERY_STATUS_EXCHANGE = ''
CELERY_TASK = ''
CELERY_MESSAGE = {"id": '', "task": CELERY_TASK, "kwargs": {"task": '', "status": ""}}

MONGO_HOST = ''
MONGO_DB = ''
MONGO_URI = 'mongodb://{}/{}'.format(MONGO_HOST, MONGO_DB)
MONGO_REQUEST_COLLECTION = ''


# See http://docs.python-cerberus.org/en/stable/usage.html
JSON_ALLOW_UNKNOWN = True
MAX_SITES_RETRIEVE = 100

JSON_SCHEMA = {
    'app': {
        'type': 'string',
        'required': True,
        'minlength': 1,
        'maxlength': 255
    },
    'app_id': {
        'type': 'integer',
        'required': False
    },
    'requests': {
        'type': 'list',
        'schema': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 2500
        },
        'required': True
    },
    'force_fetch': {
        'type': 'boolean',
        'required': False,
    },
    'images': {
        'type': 'boolean',
        'required': False
    },
    # 'wait': {
    #    'type': 'boolean',
    #    'required': False
    # }
}

try:
    from .settings_deploy import *
except ImportError:
    pass
try:
    from .settings_local import *
except ImportError:
    pass
