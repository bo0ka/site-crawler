# -*- coding: utf-8 -*-
import re
import json
import logging
import asyncio
import datetime
from aiohttp import web
from bson.json_util import dumps
from bson import ObjectId

# import xmltodict

from core import routes, setup_logging
# from apps.bots.site_crawler_bot.bot import RequestException
# from apps.bots.yandex_xml_bot.settings import LIMITS_URL
from .validation import Validator, ValidationException

loop = asyncio.get_event_loop()

setup_logging()
logger = logging.getLogger(__name__)


class Routes(routes.Routes):

    @routes.route('/', 'GET')
    async def create_task(self, request, api):
        return web.Response(body=str.encode(dumps({'status': 'ok'})))

    @routes.route('/', 'POST')
    async def create_task(self, request, api):
        task = None
        try:
            task = await api.create_api_task(request)
            loop.create_task(api.run(task))
        except ValidationException as e:
            return web.Response(body=str.encode(dumps(e.error_dict)))
        except Exception as e:
            logger.exception('[{}] [{}] :'.format(api.get_name(), str(task['_id']) if task else None))
            return web.Response(body=str.encode(dumps({
                'status': 'error',
                'task': str(task['_id']),
                'error_message': e
            })))
            # raise web.HTTPInternalServerError()
        return web.Response(body=str.encode(dumps({'status': 'ok', 'task': str(task['_id'])})))
