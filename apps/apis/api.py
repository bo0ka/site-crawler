# -*- coding: utf-8 -*-
import asyncio
import logging
import traceback

from aiohttp import web
from aiozmq import rpc
from bson.json_util import dumps

from core import setup_logging, BaseAPI, Celery

from . import NAME, settings
from .validation import Validator, ValidationException
from .routes import Routes


setup_logging()
logger = logging.getLogger(__name__)

loop = asyncio.get_event_loop()


class API(BaseAPI):

    xml_rpc_client = None
    celery = None

    def __init__(self, app):
        super(API, self).__init__(app, settings)
        self.validator = Validator()
        self.celery = Celery(app, self)

    async def initialize(self):
        await super(API, self).initialize()
        logger.info('[{}] Create RPC client connection to Yandex XML BOT [{}]...'.format(self.get_name(),
                                                                                         settings.XML_RPC_CLIENT))
        self.xml_rpc_client = await rpc.connect_rpc(connect=settings.XML_RPC_CLIENT)
        await self.celery.initialize()
        await self.add_routes(Routes())

    def get_name(self):
        return NAME

    def get_request_type(self):
        return 'POST'

    def get_route(self):
        return '/api/'+self.app.settings.API_VERSION+'/site_crawler'

    async def close(self):
        await self.celery.close()

    async def run(self, task):
        status = 'complete'

        results = []
        for r in task['requests']:
            result = await self.call(r, task)
            results.append(result)
            if not result['status'] == 'complete':
                status = 'complete with errors'

        # results = await asyncio.gather(*[self.call(r, task) for r in task['requests']])
        # if len([r for r in results if not r['status'] == 'complete']):
        #     status = 'complete with errors'

        await self.app.api_task_collection.update(
            {'_id': task['_id']},
            {'$set': {'status': status}})
        logger.info('[{}] [{}] : Task {}'.format(self.get_name(), str(task['_id']), status))
        loop.create_task(self.celery.send({
            'id': str(task['_id']),
            'status': status
        }))

    async def call(self, request, task):

        crawler_requests = [{
                                'from': self.get_name(),
                                'request': site['url'],
                                'parser': 'site',
                                'request_from': self.get_name(),
                                'request_task': str(request['_id']),
                                'force_fetch': request['force_fetch_sites'],
                                'images': request['images']
                            } for site in request['data']['sites']]

        crawler_requests.extend([{
                                     'from': self.get_name(),
                                     'request': site['saved_copy_url'],
                                     'parser': 'saved_copy',
                                     'request_from': self.get_name(),
                                     'request_task': str(request['_id']),
                                     'force_fetch': request['force_fetch_sites'],
                                     'images': False
                                 } for site in request['data']['sites']])
        result = await self.xml_rpc_client.call.run({
            'request_from': self.get_name(),
            'request_task': str(request['_id']),
            'requests': crawler_requests
        })

        # params = {'from': self.get_name(),
        #           'request_task': str(task['_id']),
        #           'request': request,
        #           'sites_count': task['sites_count'],
        #           'sites_fetch': task['sites_fetch'],
        #           'force_fetch_sites': task['force_fetch_sites'],
        #           'force_fetch': task['force_fetch'],
        #           'images': task['images']}
        # result = None
        # try:
        #     result = await self.xml_rpc_client.call.run(params)
        #
        #     if result.get('errors'):
        #         result['status'] = 'error'
        #         await self.app.api_task_collection.update(
        #             {'_id': task['_id']},
        #             {'$push': {'requests_error': {'request': request,
        #                                           'status': 'error',
        #                                           'message': result['message'],
        #                                           'errors': result['errors']}}}
        #         )
        #         logger.info('[{}] [{}] : Task error: [{}], errors: [{}]'.format(self.get_name(), str(task['_id']),
        #                                                                         result['message'], result['errors']))
        #     else:
        #         if result['status'] == 'complete':
        #             await self.app.api_task_collection.update(
        #                 {'_id': task['_id']},
        #                 {'$push': {'requests_complete': {'request': result['request'],
        #                                                  'worker_task_id': result['worker_task_id']}}}
        #             )
        #         else:
        #             await self.app.api_task_collection.update(
        #                 {'_id': task['_id']},
        #                 {'$push': {'requests_error': {'request': result['request'],
        #                                               'worker_task_id': result['worker_task_id']}}}
        #             )
        # except Exception:
        #     logger.exception('Error: ')
        return result
