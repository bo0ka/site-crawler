# -*- coding: utf-8 -*-

from settings import *

BOT_DIR = os.path.dirname(__file__)

CONFIG_DIR = os.path.join(BOT_DIR, 'config')

BOT_RPC_SERVER = 'tcp://0.0.0.0:5558'

MONGO_HOST = '192.168.101.235'
MONGO_DB = 'site_crawler'
MONGO_URI = 'mongodb://{}/{}'.format(MONGO_HOST, MONGO_DB)
MONGO_REQUEST_COLLECTION = 'request'
MONGO_FS_DB = 'site_crawler_fs'
MONGO_FS_URI = 'mongodb://{}/{}'.format(MONGO_HOST, MONGO_FS_DB)

USE_TOR = False
TOR_WORKERS_COUNT = 10
MAX_TOR_RESCAN_RETRIES = 1

PROXY_CONFIG = os.path.join(CONFIG_DIR, 'proxy_list_deploy.json')