# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import re
import datetime
from urllib.parse import urljoin

import logging
from core.log import setup_logging
setup_logging()
logger = logging.getLogger(__name__)


class SiteParser(object):
    parser = None
    raw_data = None
    result = None
    
    def __init__(self, parser=None, raw_data=None, url=None, get_images=False, from_app=None):
        self.url = url
        self.parser = parser
        self.from_app = from_app
        self.raw_data = raw_data
        self.get_images = get_images
        if self.raw_data:
            self.doc = BeautifulSoup(self.raw_data)
        else:
            self.doc = None

    def parse(self):
        if self.parser == 'site':
            return self.parse_site()
        else:
            return self.parse_saved_copy_date()

    def parse_site(self):
        keywords = ''
        description = ''
        title = ''
        h1 = []
        imgs = []
        if self.doc:
            h1 = self.get_tags('h1')
            title = self.get_tags('title')
            description = self.get_meta('description')
            keywords = self.get_meta('keywords')
            imgs = self.get_imgs()
        return {
            'keywords': keywords,
            'description': description,
            'title': title,
            'h1': h1,
            'imgs_absolute_urls': imgs
        }

    def get_tags(self, tag):
        data = None
        result = self.doc.select(tag)
        if len(result):
            if result[0].text:
                data = result[0].text.strip()
        return data

    def get_imgs(self):
        return list(set([urljoin(self.url, i.get('src')) for i in self.doc.select('img')]))

    def get_meta(self, tag):
        data = None
        result = self.doc.select('meta')
        for r in result:
            if (r.attrs.get('name') and r.attrs.get('content')) and r.attrs.get('name') == tag:
                data = r.attrs.get('content').strip()
        return data

    def parse_saved_copy_date(self):
        saved_date = None
        if self.from_app == "Google Search BOT":
            months = ['янв', 'фев', 'мар', 'апр', 'мая', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек']
            m = re.search("снимок страницы по состоянию на (.+?)\. </div>", self.raw_data)
            if m:
                datestr = m.group(1)
                date_arr = datestr.replace(' GMT', '').split(' ')
                _date_str = '{}.0{}.{} {}'.format(date_arr[0], months.index(date_arr[1]), date_arr[2], date_arr[3])
                saved_date = datetime.datetime.strptime(_date_str, '%d.%m.%Y %H:%M:%S')
        else:
            if self.doc:
                m = re.search("\)\)\.date = (.+?)';", self.raw_data)
                if m:
                    datestr = m.group(1)
                    datestr = datestr.replace("\\", "").replace("'", "")
                    saved_date = datetime.datetime.strptime(datestr, '%d.%m.%Y [%H:%M:%S]')
        return {'saved_date': saved_date}

