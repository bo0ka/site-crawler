# -*- coding: utf-8 -*-

from settings import *

BOT_RPC_SERVER = 'tcp://0.0.0.0:5558'

BOT_DIR = os.path.dirname(__file__)

CONFIG_DIR = os.path.join(BOT_DIR, 'config')

MONGO_HOST = ''
MONGO_DB = ''
MONGO_URI = 'mongodb://{}/{}'.format(MONGO_HOST, MONGO_DB)
MONGO_REQUEST_COLLECTION = ''

# See http://docs.python-cerberus.org/en/stable/usage.html
JSON_ALLOW_UNKNOWN = False

JSON_SCHEMA = {
    'from': {
        'type': 'string',
        'required': True,
        'minlength': 1,
        'maxlength': 255
    },
    'request': {
        'type': 'string',
        'required': True,
        'minlength': 1,
        'maxlength': 2500
    },
    'parser': {
        'type': 'string',
        'required': True,
        'allowed': ['site', 'saved_copy']
    },
    'request_from': {
        'type': 'string',
        'required': True,
        'minlength': 1,
        'maxlength': 500
    },
    'request_task': {
        'type': 'string',
        'required': True,
        'minlength': 1,
        'maxlength': 255
    },
    'force_fetch': {
        'type': 'boolean',
        'required': True
    },
    'images': {
        'type': 'integer',
        'required': True
    },
    'rescan': {
        'type': 'boolean',
        'required': False
    },
    'retry': {
        'type': 'integer',
        'required': False
    },
    'last_worker': {
        'type': 'integer',
        'required': False
    }
}

MAX_REQUESTS = 30
MAX_REQUESTS_TOR_PROXY = 10
MAX_REQUESTS_HTTP_PROXY = 10

MAX_TOR_RESCAN_RETRIES = 4

HEADERS = {
    'user-agent:': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36'
}


USE_TOR = True

BASE_SOCKS_PORT = 9051
BASE_PRIVOXY_PORT = 8300
BASE_CONTROL_PORT = 8119

TOR_BASE_DIR = os.path.join(BOT_DIR, 'tordata')
if not os.path.isdir(TOR_BASE_DIR):
    os.mkdir(TOR_BASE_DIR)

TOR_CONTROLLER_PASSWORD = '123456'

PROXY_CONFIG = os.path.join(CONFIG_DIR, 'proxy_list.json')
PRIVOXY_SEED = os.path.join(CONFIG_DIR, 'privoxy_seed')
TOR_SEED = os.path.join(CONFIG_DIR, 'torrc_seed')

try:
    from .settings_deploy import *
except ImportError:
    pass
try:
    from .settings_local import *
except ImportError:
    pass
