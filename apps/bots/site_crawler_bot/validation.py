from core.validation import ValidatorBase, ValidationException
from .settings import JSON_SCHEMA, JSON_ALLOW_UNKNOWN


class Validator(ValidatorBase):
    def __init__(self):
        super(Validator, self).__init__(json_schema=JSON_SCHEMA,
                                        json_allow_unknown=JSON_ALLOW_UNKNOWN, request_type='bot')

    async def valid_params(self, request):
        params = await self.validate_request(request)
        if not self.jv.validate(params):
            raise ValidationException(self.jv.errors, self.json_schema)
        if not params.get('rescan'):
            params.update({
                'retry': 0
            })
        return params
