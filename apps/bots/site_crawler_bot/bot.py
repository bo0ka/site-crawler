# -*- coding: utf-8 -*-

import asyncio
import aiohttp
from aiozmq import rpc
from concurrent.futures import ProcessPoolExecutor

from motor.motor_asyncio import AsyncIOMotorGridFSBucket, AsyncIOMotorClient

import subprocess
import datetime
import yaml
import json
import shutil
from urllib.parse import urlparse
from bson import ObjectId

import os
import logging
import traceback
import random

from . import NAME, settings

from core import setup_logging, BaseAPI, MongoRequestMixin, RequestException
from .parser import SiteParser
from .validation import Validator, ValidationException

setup_logging()
logger = logging.getLogger(__name__)

executor = ProcessPoolExecutor()
loop = asyncio.get_event_loop()


class Proxy(object):
    proxy_list = []
    proxies_config = None
    number = 0

    def __init__(self, proxies_config=None, use_tor=settings.USE_TOR, config=None):
        if not config:
            if use_tor:
                for i in range(settings.TOR_WORKERS_COUNT):
                    self.__class__.proxy_list.append(Proxy(config={'type': 'tor', 'number': self.__class__.number}))
                    self.__class__.number += 1
            if proxies_config:
                self.__class__.proxies_config = self.__class__.load_config(proxies_config)
                for _, pc in self.__class__.proxies_config.items():
                    pc.update({'number': self.__class__.number})
                    self.__class__.proxy_list.append(Proxy(config=pc))
                    self.__class__.number += 1
            logger.info('proxy list: {}'.format(self.__class__.proxy_list))
        else:
            self.ptype = None
            self.host = None
            self.port = None
            self.login = None
            self.password = None
            self.proxy_str = None

            self.tor_pid = None
            self.tor_socks_port = None
            self.tor_control_port = None
            self.tor_workdir = None
            self.tor_pidfile = None
            self.tor_http_proxy_port = None
            self.tor_privoxy_dir = None
            self.tor_privoxy_pidfile = None

            self.setup_proxy(config)

    def get_name(self):
        return '{} proxy #{}'.format(self.ptype.upper(), self.number)

    @classmethod
    def load_config(self, config_file):
        try:
            with open(config_file, mode='r') as f:
                #loaded_config = yaml.load(f)
                loaded_config = json.load(f)
                logger.info("proxy config: {}".format(loaded_config))
                return loaded_config
        except Exception as e:
            logger.exception("Can't load proxy config.")

    def setup_proxy(self, config):
        self.close()
        self.number = config['number']
        self.ptype = config.get('type', None)
        self.host = config.get('host', None)
        self.port = config.get('port', None)
        self.login = config.get('login', None)
        self.password = config.get('password', None)

        if self.ptype == 'tor':
            self.host = '127.0.0.1'
            self.tor_workdir = os.path.join(settings.TOR_BASE_DIR, 'tor{}'.format(self.number))
            self.tor_socks_port = settings.BASE_SOCKS_PORT + self.number
            self.tor_control_port = settings.BASE_CONTROL_PORT + self.number
            self.tor_pidfile = 'tor{}.pid'.format(self.number)
            self.tor_http_proxy_port = settings.BASE_PRIVOXY_PORT + self.number
            self.tor_privoxy_dir = os.path.join(self.tor_workdir, 'privoxy')
            self.tor_privoxy_pidfile = 'privoxy{}.pid'.format(self.number)
            self.start_tor()
            self.start_privoxy()
            self.proxy_str = 'http://{}:{}'.format(self.host, self.tor_http_proxy_port)

        if self.ptype == 'http':
            self.proxy_str = 'http://{}:{}'.format(self.host, self.port)

        logger.info('[{}] Ready: [{}]'.format(self.get_name(), self.proxy_str))

    def start_tor(self):
        if not os.path.isdir(self.tor_workdir):
            os.makedirs(self.tor_workdir)
        torrc_path = os.path.join(self.tor_workdir, 'torrc')
        shutil.copy(os.path.join(settings.CONFIG_DIR, 'torrc_seed'), torrc_path)
        subprocess.run([
            '/usr/bin/tor',
            '-f', torrc_path,
            '--RunAsDaemon', "1",
            '--CookieAuthentication', "0",
            '--HashedControlPassword', "",
            '--ControlPort', str(self.tor_control_port),
            '--PidFile', self.tor_pidfile,
            '--SocksPort', str(self.tor_socks_port),
            '--DataDirectory', self.tor_workdir],
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    def start_privoxy(self):
        shutil.copytree(settings.PRIVOXY_SEED, self.tor_privoxy_dir)
        with open(os.path.join(self.tor_privoxy_dir, 'config'), 'r') as src:
            src_cfg = src.readlines()
        dst_cfg = []
        for l in src_cfg:
            if l.startswith('forward-socks5'):
                l = 'forward-socks5 / 127.0.0.1:{} .\n'.format(self.tor_socks_port)
            if l.startswith('confdir'):
                l = 'confdir {}\n'.format(self.tor_privoxy_dir)
            if l.startswith('listen-address'):
                l = 'listen-address  127.0.0.1:{}\n'.format(self.tor_http_proxy_port)
            if l.startswith('logdir'):
                l = 'logdir {}\n'.format(os.path.join(self.tor_privoxy_dir, 'log'))
            dst_cfg.append(l)
        with open(os.path.join(self.tor_privoxy_dir, 'config'), 'w') as dst:
            for l in dst_cfg:
                dst.write(l)
            dst.truncate()

        subprocess.run([
            '/usr/sbin/privoxy',
            '--pidfile', os.path.join(self.tor_privoxy_dir, self.tor_privoxy_pidfile),
            os.path.join(self.tor_privoxy_dir, 'config')],
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    def close(self):
        for p in self.__class__.proxy_list:
            logger.info('[{}] Close'.format(p.get_name()))
            if p.ptype == 'tor':
                torpid = p.get_pid()
                if torpid:
                    subprocess.run(['kill', torpid], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                privoxypid = p.get_pid(who='privoxy')
                if privoxypid:
                    subprocess.run(['kill', privoxypid], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                if os.path.isdir(p.tor_workdir):
                    shutil.rmtree(p.tor_workdir)
        if os.path.isdir(settings.TOR_BASE_DIR):
            shutil.rmtree(settings.TOR_BASE_DIR)

    def get_pid(self, who='tor'):
        pid = None
        if who == 'tor':
            pid_path = os.path.join(self.tor_workdir, self.tor_pidfile)
        elif who == 'privoxy':
            pid_path = os.path.join(self.tor_privoxy_dir, self.tor_privoxy_pidfile)
        else:
            return None
        if os.path.isfile(pid_path):
            with open(pid_path) as f:
                pid = f.read().strip()
        return pid


class BOT(rpc.AttrHandler, MongoRequestMixin, BaseAPI):

    workers = []

    def __init__(self, app):
        super(BOT, self).__init__(app, settings)

        self.mongo_fs_client_request = AsyncIOMotorClient(self.settings.MONGO_FS_URI)
        self.mongo_fs_db_request = self.mongo_fs_client_request.get_database(self.settings.MONGO_FS_DB)

        self.gridfs = AsyncIOMotorGridFSBucket(self.mongo_fs_db_request)
        self.validator = Validator()
        self.proxy = Proxy(proxies_config=settings.PROXY_CONFIG)
        # self.proxy = None
        self.finish = False

    async def initialize(self):
        await super(BOT, self).initialize()
        logger.info('[{}] Start serving RPC requests at [{}]...'.format(self.get_name(), settings.BOT_RPC_SERVER))
        await rpc.serve_rpc(self, bind=settings.BOT_RPC_SERVER)
        await self.setup_workers()

    def get_name(self):
        return NAME

    def get_request_type(self):
        pass

    async def close(self):
        self.finish = True
        await loop.run_in_executor(executor, self.proxy.close)

    async def setup_workers(self):
        # one worker per proxy
        number = 0
        for p in self.proxy.proxy_list:
            worker = Worker(number, proxy=p)
            self.__class__.workers.append(worker)
            number += 1

    async def get_worker(self, request, proxy_type=None, bad_worker=None, random_worker=False):
        workers = []

        if proxy_type:
            _workers = [w for w in self.__class__.workers if w.proxy.ptype == proxy_type]
        else:
            _workers = [w for w in self.__class__.workers if not w.proxy]
        for w in _workers:
            if request in w.todo:
                continue
            else:
                if bad_worker and w.number == bad_worker:
                    continue
                else:
                    workers.append(w)

        if len(workers) == 0:
            await asyncio.sleep(0.5)
            workers = self.__class__.workers

        if not random_worker:
            less_busy_worker = min(workers, key=lambda w: len(w.todo))
        else:
            less_busy_worker = random.choice(workers)

        return less_busy_worker

    @rpc.method
    async def run(self, request_data):
        results = []
        try:
            logger.info("run start...")
            results = await asyncio.gather(*[self.run_request(r) for r in request_data['requests']],
                                           return_exceptions=True)
            post_results = await self.post_run(results)
            for pr in [pr for pr in post_results if pr]:
                for r in [r for r in results if r]:
                    if pr['request'] == r['request']:
                        r.update(pr)
        except Exception:
            logger.exception('# Pages error: ')
        # TODO: кажется на этом сайте была проблема с определением кодировки
        # results = [{'images': True, 'parser': 'site', 'worker_task_id': '57971562a33fe23b77b88028',
        #                    'request': 'http://www.medical-enc.ru/10/klimaks.shtml', 'message': None,
        #                    'status': 'complete'}]
        try:
            await self.get_images(results)
        except Exception:
            logger.exception('# Images error: ')
        logger.exception('run stop')
        return results

    async def get_img(self, src):
        logger.info('get_image start {}'.format(src))
        fname = src.rsplit('/')[-1] if len(src.rsplit('/')) else None
        fid, data = None, None
        if fname:
            try:
                gridout = await self.gridfs.get_last_version(src=src)
            except Exception as e:
                gridout = None

            if gridout:
                fid = gridout._id
                fname = gridout.name
                src = gridout.src
                logger.info('image already done: {}'.format(src))
            else:
                worker = await self.get_worker(src, proxy_type='tor', random_worker=True)

                data, error, start, total = await worker.fetcher(src)

                if data:
                    gridin = await self.gridfs.new_file()
                    await gridin.write(data)
                    await gridin.set('name', fname)
                    await gridin.set('src', src)
                    await gridin.close()
                    fid = gridin._id

                logger.info('[{}] get image done: {}'.format(worker.get_name(), src))
        logger.info('get_image stop {}'.format(src))
        return {
            '_id': fid,
            'name': fname,
            'src': src
        }

    async def get_todo_img_src_for(self, request):
        new_urls = request['data']['site_data']['imgs_absolute_urls']
        downloaded_images = request['data']['site_data']['images']
        _dimgs = [i['src'] for i in downloaded_images]
        todo = []
        for nu in new_urls:
            if nu not in _dimgs:
                todo.append(nu)
        return todo

    async def get_images(self, requests):
        try:
            images_src = []
            db_reqs = []

            _good_req = [r for r in requests if isinstance(r, dict)]

            for req in [r for r in _good_req if r.get('status') and r.get('request') and r.get('images')]:
                dbreq = await self.request_collection.find_one({'_id': ObjectId(req['worker_task_id'])})

                if dbreq.get('data') and dbreq['data'].get('site_data') and \
                        dbreq['data']['site_data'].get('imgs_absolute_urls'):
                    db_reqs.append(dbreq)
                    todo_urls = await self.get_todo_img_src_for(dbreq)
                    images_src.extend(todo_urls)

                images_src = list(set(images_src))

            _images = await asyncio.gather(*[self.get_img(s) for s in images_src], return_exceptions=True)

            _good_images = [i for i in _images if isinstance(i, dict) and not i['_id'] == None]

            for req in db_reqs:
                images = []
                for imgsrc in req['data']['site_data']['imgs_absolute_urls']:
                    for i in _good_images:
                        if imgsrc == i['src']:
                            images.append(i)
                await self.request_collection.update({'_id': req['_id']},
                                                     {'$set': {'data.site_data.images': images}})
        except Exception:
            logger.exception('Err: ')

    async def get_bad_requests(self, requests):
        bad_requests = []
        # for r in requests:
        #     if not r.get('status'):
        #         print(r)
        if requests:
            requests = [r for r in requests if r and r.get('status') and r.get('request')]
            for req in [r for r in requests if not r['status'] == 'complete']:
                breq = await self.request_collection.find_one({'_id': ObjectId(req['worker_task_id'])})
                bad_requests.append(breq)
        return bad_requests

    async def post_run(self, requests):
        logger.info("post_run start...")
        retry = 0
        good_requests = []
        bad_requests = await self.get_bad_requests(requests)
        while len(bad_requests) and retry < settings.MAX_TOR_RESCAN_RETRIES:
            logger.info('run rescan retry # {}'.format(retry))
            results = await asyncio.gather(*[self.run_request(r, rescan=True) for r in bad_requests])
            good_requests.extend([r for r in results if r['status'] == 'complete'])
            retry += 1
            bad_requests = await self.get_bad_requests(results)
            # await asyncio.sleep(random.uniform(1.1, 3.4))
        if len(bad_requests):
            logger.info('run rescan good proxy')
            results = await asyncio.gather(*[self.run_request(r, rescan=True, proxy_type='http') for r in bad_requests])
            good_requests.extend([r for r in results if r['status'] == 'complete'])
        logger.info("post_run stop...")
        return good_requests

    async def create_request(self, request_data):
        logger.info("create_request start {}".format(request_data))
        import datetime

        logger.info("\tvalid_params")
        request_data = await self.validator.valid_params(request_data)
        logger.info("\tget_scanned")
        scanned_request = await self.get_scanned(request_data, additional_params=None)
        scanned = False

        if scanned_request:
            logger.info("\tif scanned_request")
            request = scanned_request
            if request['status'] == 'fetching sites' or request['status'] == 'complete' or request_data['force_fetch']:
                if not request['status'] == 'complete':
                    logger.warning('[{}] [{}] : Request scanned at week: [{}], but not complete: [{}], '
                                   'current request task: [{}]'
                                   .format(self.get_name(), str(request['_id']), request['request'], request['status'],
                                           request_data['request_task']))

                elif request_data['force_fetch']:
                    logger.warning('[{}] [{}] : Request scanned at week: [{}] status: [{}], but task requested rescan, '
                                   'current request task: [{}]'
                                   .format(self.get_name(), str(request['_id']), request['request'], request['status'],
                                           request_data['request_task']))
                else:
                    pass

                _id = request['_id']
                orig_request_task = request['request_task']

                logger.info("\trequest.update 1")
                request.update(request_data)
                logger.info("\trequest.update 2")
                request.update({'request_task': orig_request_task, 'mod_date': datetime.datetime.now()})

                logger.info("\tdel request")
                del request['_id']
                logger.info("\trequest_collection.update")
                await self.request_collection.update(
                    {'_id': _id},
                    {'$set': request}
                )
                logger.info("\trequest_collection.find_one")
                request = await self.request_collection.find_one({'_id': _id})
            else:
                scanned = True
                logger.warning('[{}] [{}] : Request scanned at week: [{}], current request task: [{}]'
                               .format(self.get_name(), str(request['_id']), request['request'], request_data['request_task']))
        else:
            logger.info("\tif scanned_request - else...")
            logger.info("\trequest_data.update")
            request_data.update({'data': None, 'status': 'process', 'message': None,
                                 'date': datetime.datetime.now()})
            logger.info("\trequest_collection.insert")
            request = await self.request_collection.insert(request_data)
            logger.info("\trequest_collection.find_one")
            request = await self.request_collection.find_one({'_id': request})
            logger.info('[{}] [{}] : Request created: [{}], from api task: [{}]'
                        .format(self.get_name(), str(request['_id']), request['request'], request['request_task']))
        logger.info("create_request stop")
        return request, scanned

    async def run_request(self, request_data, rescan=False, proxy_type='tor'):
        logger.info("run_request start")
        if not rescan:
            try:
                request, scanned = await self.create_request(request_data)
            except ValidationException as e:
                e.error_dict.update({'request': request_data.get('request')})
                return e.error_dict

            if not scanned or request['force_fetch']:
                try:
                    if request['from'] == "Google Search BOT" and request['parser'] == 'saved_copy':
                        worker = await self.get_worker(request['request'], proxy_type='http')
                    else:
                        worker = await self.get_worker(request['request'], proxy_type=proxy_type)
                    request = await worker.fetch_page(request)

                    await self.request_collection.update(
                        {'_id': request['_id']},
                        {'$set': {'data': request['data'], 'status': request['status'], 'message': request['message'],
                                  'start': request['start'], 'total': request['total'], 'retry': request['retry'],
                                  'last_worker': request['last_worker']}}
                    )
                    request = await self.request_collection.find_one({'_id': request['_id']})
                    return ({
                        'request': request['request'],
                        'parser': request['parser'],
                        'status': request['status'],
                        'message': request['message'],
                        'worker_task_id': str(request['_id']),
                        'images': request['images']
                    })
                except Exception as e:
                    logger.exception('Error: ')
        else:
            request = request_data
            if request['from'] == "Google Search BOT" and request['parser'] == 'saved_copy':
                worker = await self.get_worker(request['request'], bad_worker=request['last_worker'], proxy_type='http')
            else:
                worker = await self.get_worker(request['request'], bad_worker=request['last_worker'], proxy_type=proxy_type)

            request = await worker.fetch_page(request)

            await self.request_collection.update(
                {'_id': request['_id']},
                {'$set': {'data': request['data'], 'status': request['status'], 'message': request['message'],
                          'start': request['start'], 'total': request['total'], 'retry': request['retry'],
                          'last_worker': request['last_worker']}}
            )
            request = await self.request_collection.find_one({'_id': request['_id']})
            return ({
                'request': request_data['request'],
                'parser': request_data['parser'],
                'status': request_data['status'],
                'message': request_data['message'],
                'worker_task_id': str(request_data['_id']),
                'images': request['images']
            })
        logger.info("run_request stop")


class Worker(BaseAPI):

    def __init__(self, number, proxy=None):
        self.proxy = proxy
        if self.proxy.ptype == 'tor':
            max_requests = settings.MAX_REQUESTS_TOR_PROXY
        else:
            max_requests = settings.MAX_REQUESTS_HTTP_PROXY

        super(Worker, self).__init__(None, settings, max_requests=max_requests)

        self.proxy_auth = None
        if self.proxy.login and self.proxy.password:
            self.proxy_auth = aiohttp.BasicAuth(self.proxy.login, self.proxy.password)

        self.number = number
        self.todo = []
        self.last_request = {}

    def get_name(self):
        if self.proxy:
            return "Crawler {} worker #{}".format(self.proxy.ptype.upper(), self.number)
        else:
            return "Crawler worker #{}".format(self.number)

    def get_request_type(self):
        pass

    async def close(self):
        pass

    async def fetcher(self, url):
        domain = urlparse(url).netloc
        data, error, start, total = None, None, None, None
        try:
            # throttling for yandex
            # if domain == 'hghltd.yandex.net':
            #     await asyncio.sleep(random.uniform(1.1, 3.4))

            # TODO: ? async with self.semaphore
            with (await self.semaphore):
                data, start, total = await self.get(url, None, time_it=True, last_request=self.last_request,
                                                    proxy=self.proxy.proxy_str, proxy_auth=self.proxy_auth)
            # connector = aiohttp.proxy(self.proxy.proxy_str,
            #                           proxy_auth=self.proxy_auth,
            #                           verify_ssl=False)
            #
            # with (await self.semaphore):
            #     with aiohttp.Timeout(self.http_timeout):
            #         data, start, total = await self.get(url, None, time_it=True, last_request=self.last_request,
            #                                             connector=aiohttp.ProxyConnector(self.proxy.proxy_str,
            #                                                                              proxy_auth=self.proxy_auth,
            #                                                                              verify_ssl=False))

        # except asyncio.CancelledError as e:
        #     print("---------------------------------------------------------- cancelled ")
        #     error = {'message': str(e), 'code': None}
        #     start, total = None, None
        #
        # except asyncio.TimeoutError as e:
        #     print("---------------------------------------------------------- time out")
        #     error = {'message': str(e), 'code': None}
        #     start, total = None, None

        except RequestException as e:
            error = {'message': e.message, 'code': e.code}
            start, total = e.start, e.total

        except Exception as e:
            error = {'message': str(repr(e)), 'code': None}
            start, total = None, None

        self.last_request = {'start': start, 'domain': domain}

        return data, error, start, total

    async def fetch_page(self, request):
        self.todo.append(request['request'])

        data, error, start, total = await self.fetcher(request['request'])

        if data:
            data = self.decode_data(data)

        status = 'fetched' if not error else 'error'
        message = None if not error else error
        site_data = None
        saved_copy_data = None
        if error:
            status = 'request error'
            logger.warning('[{} started: {}, total: {}] [{}] : Request error from [{}] request task id: [{}], url: [{}], '
                         'Error: {}'.format(self.get_name(), start, total, str(request['_id']), request['request_from'],
                                            request['request_task'], request['request'], message))
        else:
            try:
                parsed_data = SiteParser(parser=request['parser'], raw_data=data, url=request['request'],
                                         from_app=request['from'], get_images=request['images']).parse()
                if request['parser'] == 'site':
                    site_data = parsed_data
                    if not site_data.get('images'):
                        site_data.update({'images': []})
                if request['parser'] == 'saved_copy':
                    saved_copy_data = parsed_data
                status = 'complete'
                data = {'page': data, 'site_data': site_data, 'saved_copy_data': saved_copy_data}
                logger.info('[{} started: {}, total: {}] [{}] : Request done from [{}] request task id: [{}], url: [{}]'
                            .format(self.get_name(), start, total, str(request['_id']), request['request_from'],
                                    request['request_task'], request['request']))
            except Exception as e:
                status = 'parser error'
                message = 'Parser error: {}'.format(repr(str(e)))
                logger.warning('[{}] [{}] : Parser error from [{}] request task id: [{}], url: [{}], '
                             'Traceback: {}'.format(self.get_name(), str(request['_id']), request['request_from'],
                                                    request['request_task'], request['request'], traceback.format_exc()))

        request.update({'data': data, 'status': status, 'message': message, 'start': start, 'total': total,
                        'retry': request['retry'] + 1, 'last_worker': self.number})
        self.todo.remove(request['request'])
        return request

    async def fetch_image(self, src, request):

        fname, fid, data = src.rsplit('/')[-1] if len(src.rsplit('/')) else None, None, None

        if fname:
            self.todo.append(src)

            data, error, start, total = await self.fetcher(src)

            if data:
                logger.info('[{} started: {}, total: {}] [{}] : Request done from [{}] request task id: [{}], url: [{}]'
                            .format(self.get_name(), start, total, str(request['_id']), request['request_from'],
                                    request['request_task'], src))
            else:
                logger.warning(
                    '[{} started: {}, total: {}] [{}] : Request error from [{}] request task id: [{}], url: [{}], '
                    'Error: {}'.format(self.get_name(), start, total, str(request['_id']), request['request_from'],
                                       request['request_task'], src, error))

            self.todo.remove(src)

        return {
            '_id': fid,
            'name': fname,
            'src': src,
            'data': data
        }
